package main

import (
	nested "github.com/antonfisher/nested-logrus-formatter"
	"github.com/sirupsen/logrus"
	"gitlab.com/TigorLazuardi/reddit-image-downloader/cli"
)

func main() {
	err := cli.CMD.Execute()
	if err != nil {
		logrus.Fatal(err)
	}
}

func init() {
	logrus.SetFormatter(&nested.Formatter{
		ShowFullLevel:   true,
		TrimMessages:    true,
		TimestampFormat: "[2006-01-02 15:04:05]",
		CallerFirst:     true,
	})
	logrus.SetLevel(logrus.InfoLevel)
}
