module gitlab.com/TigorLazuardi/reddit-image-downloader

go 1.16

require (
	github.com/afex/hystrix-go v0.0.0-20180502004556-fa1af6a1f4f5 // indirect
	github.com/antonfisher/nested-logrus-formatter v1.3.1 // indirect
	github.com/avast/retry-go v3.0.0+incompatible // indirect
	github.com/sirupsen/logrus v1.8.1 // indirect
	github.com/spf13/cobra v1.1.3
)
