package cli

import (
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

var CMD = &cobra.Command{
	Short: "CLI program to download reddit images",
	Use:   "rid",
}

func init() {
	cobra.OnInitialize(loadConfig)
	CMD.PersistentFlags().BoolP("verbose", "v", false, "runs the program with debug logging")
}

func loadConfig() {
	b, _ := CMD.Flags().GetBool("verbose")
	if b {
		logrus.SetLevel(logrus.DebugLevel)
	}
}
