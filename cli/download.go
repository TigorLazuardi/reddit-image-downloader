package cli

import (
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

var downloadCMD = &cobra.Command{
	Use:     "download",
	Short:   "download command for images",
	Example: "rid download fantasymoe thighdeology",
	Run: func(cmd *cobra.Command, args []string) {
		logrus.Info("uhuh")
		logrus.Info(args)
	},
}

func init() {
	CMD.AddCommand(downloadCMD)
}
