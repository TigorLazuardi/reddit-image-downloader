package repository

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"sync"
	"time"

	"github.com/afex/hystrix-go/hystrix"
	"github.com/avast/retry-go"
	"github.com/sirupsen/logrus"
	"gitlab.com/TigorLazuardi/reddit-image-downloader/app/reddit/domain"
	"gitlab.com/TigorLazuardi/reddit-image-downloader/app/reddit/models"
)

type subredditRepository struct {
	client          *http.Client
	listingCommand  string
	downloadCommand string
}

func NewSubredditRepository(client *http.Client, listingCommand string, downloadCommand string) domain.RedditRepository {
	domain.ConfigureListing(listingCommand)
	domain.ConfigureDownloader(downloadCommand)
	return subredditRepository{client: client, listingCommand: listingCommand, downloadCommand: downloadCommand}
}

func (sr subredditRepository) GetListings(ctx context.Context, subreddits map[domain.SubredditName]domain.Sorting) (map[domain.SubredditName]models.Subreddit, map[domain.SubredditName]error) {
	m := make(map[domain.SubredditName]models.Subreddit)
	errs := make(map[domain.SubredditName]error)
	wg := sync.WaitGroup{}
	wg.Add(len(subreddits))
	mutex := sync.Mutex{}
	for subreddit, sorting := range subreddits {
		go func(ctx context.Context, s domain.SubredditName, sort domain.Sorting) {
			defer wg.Done()
			sub, err := sr.getList(ctx, s, sort)
			mutex.Lock()
			m[s] = sub
			errs[s] = err
			mutex.Unlock()
		}(ctx, subreddit, sorting)
	}
	wg.Wait()
	return m, errs
}

func (sr subredditRepository) getList(ctx context.Context, subreddit domain.SubredditName, sorting domain.Sorting) (sub models.Subreddit, err error) {
	uri := fmt.Sprintf("https://www.reddit.com/r/%s/%s.json", subreddit, sorting)
	log := logrus.WithField("uri", uri)
	err = retry.Do(func() error {
		err := hystrix.Do(sr.listingCommand,
			func() error {
				log.Debug("getting list...")
				req, err := http.NewRequestWithContext(ctx, http.MethodGet, uri, nil)
				if err != nil {
					log.WithError(err).Debug("failed to create request: ", err.Error())
					return retry.Unrecoverable(err)
				}
				res, err := sr.client.Do(req)
				if err != nil {
					log.WithError(err).Debug("failed gain response from reddit: ", err.Error())
					return retry.Unrecoverable(err)
				}
				if res.StatusCode >= 400 {
					err := fmt.Errorf("got error code from reddit: %d", res.StatusCode)
					log.WithError(err).Debug(err)
					return err
				}
				err = json.NewDecoder(res.Body).Decode(&sub)
				if err != nil {
					logrus.WithError(err).Debug(err.Error())
					return retry.Unrecoverable(err)
				}
				return nil
			}, nil)
		if err == hystrix.ErrCircuitOpen || err == hystrix.ErrMaxConcurrency {
			log.WithError(err).Debug("circuit breaker error")
			err = retry.Unrecoverable(err)
		}
		return err
	}, retry.Delay(5*time.Second), retry.Attempts(3), retry.OnRetry(func(n uint, err error) {
		log.Debug("retry attempt: ", n)
	}))
	return
}
