package domain

import (
	"context"

	"gitlab.com/TigorLazuardi/reddit-image-downloader/app/reddit/models"
)

type Sorting string

const (
	SortingHot           Sorting = "hot"
	SortingNew           Sorting = "new"
	SortingRandom        Sorting = "random"
	SortingRising        Sorting = "rising"
	SortingTop           Sorting = "top"
	SortingControversial Sorting = "controversial"
)

type SubredditName string

type RedditRepository interface {
	GetListings(ctx context.Context, subreddits map[SubredditName]Sorting) (map[SubredditName]models.Subreddit, map[SubredditName]error)
}
