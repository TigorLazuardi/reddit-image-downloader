package domain

import (
	"github.com/afex/hystrix-go/hystrix"
)

func ConfigureListing(command string) {
	hystrix.ConfigureCommand(command, hystrix.CommandConfig{
		Timeout: 5000,
		// Reddit limit
		MaxConcurrentRequests: 30,
		ErrorPercentThreshold: 25,
	})
}

func ConfigureDownloader(command string) {
	hystrix.ConfigureCommand(command, hystrix.CommandConfig{
		Timeout:               5000,
		MaxConcurrentRequests: 100,
		ErrorPercentThreshold: 75,
	})
}
