package domain

import "context"

type SQLiteRepository interface {
	GetDownloadedLists(ctx context.Context, subreddits []string) (downloadlist map[string]string, err error)
	InsertDownloadList(ctx context.Context, list map[string]string)
	RemoveDownloadList(ctx context.Context, list map[string]string)
	GetSavedSubreddits(ctx context.Context) (subreddits []string, err error)
	InsertNewSubreddits(ctx context.Context, subreddits []string) (err error)
}
